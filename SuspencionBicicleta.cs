﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPatron
{
    class SuspencionBicicleta : ComponentesBicicleta
    {
        private Bicicleta currentBicicleta;

        public SuspencionBicicleta(Bicicleta bicicleta)
        {
            this.currentBicicleta = bicicleta;
        }
        public override decimal CalcularCosto()
        {
            return this.currentBicicleta.CalcularCosto() + 120.00M;
        }
    }
}

/*Se puede ver que lo que estamos cogiendo un objeto Bicicleta
    y lo estamos extendiendo para que el método CalcularCosto
    incremente en 120.00 lo que devuelve el método original.
    Para poder usar el método original, pasamos el objeto 
    Bicicleta como parámetro del constructor. (Se realiza lo mismo con los otros objetos de bicicleta)*/
