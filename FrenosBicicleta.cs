﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPatron
{
    class FrenosBicicleta : ComponentesBicicleta
    {
        private Bicicleta  currentBicicleta;

        public FrenosBicicleta(Bicicleta bicicleta)
        {
            this.currentBicicleta = bicicleta;
        }
        public override decimal CalcularCosto()
        {
            return this.currentBicicleta.CalcularCosto() + 255.20M;
        }
    }
}
