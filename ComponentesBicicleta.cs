﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPatron
{
    public abstract class ComponentesBicicleta:Bicicleta
    {
        public override abstract decimal CalcularCosto();

    }
}
/*
Esta clase hereda de la clase original
por lo que los objetos se podrán convertir a Bicicleta con un simple cast.

Para este ejemplo se crea una clase por cada componente de la bicicleta. A su vez, cada una de estas clases
hereda de ComponentesBicicleta


*/
