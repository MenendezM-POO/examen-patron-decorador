﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPatron
{
    class BaseBicicleta : Bicicleta
    {

        public override decimal CalcularCosto()
        {
            return 0M;
        }
        /*
         * En este caso se le asignó un valor de 0, pero se podrian crear otras configuraciones base
         *  como bicicletas Montañeras, de ruta, bmx, etc.
        */
    }


}
