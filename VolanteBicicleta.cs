﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPatron
{
    class VolanteBicicleta : ComponentesBicicleta
    {
        private Bicicleta currentBicicleta;

        public VolanteBicicleta(Bicicleta bicicleta)
        {
            this.currentBicicleta = bicicleta;
        }
        public override decimal CalcularCosto()
        {
            return this.currentBicicleta.CalcularCosto() + 420.00M;
        }
    }
}
