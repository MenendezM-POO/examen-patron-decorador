﻿using System;
using System.Collections.Generic;
using System.Text;


/* En Esta clase la utilizamos para que las clases que la hereden,
se vean obligadas a implementar su propio método Calcular. */


namespace ExamenPatron
{
    public abstract class Bicicleta
    {

        public abstract decimal CalcularCosto();
    }

}
