﻿using System;

namespace ExamenPatron
{
    class Program
    {
        static void Main(string[] args)
        {

            
            Bicicleta bicicletaFast = new BaseBicicleta();
            bicicletaFast = new FrenosBicicleta(bicicletaFast);
            bicicletaFast = new SuspencionBicicleta(bicicletaFast);
            bicicletaFast = new VolanteBicicleta(bicicletaFast);

            var costo = bicicletaFast.CalcularCosto();

            Console.WriteLine(string.Format("El Costo de la bicicleta Montañera es de {0} Dolares", costo));
            Console.ReadKey();
        }
    }
}


/*
Se aprovechan los objetos del CompenteBicicleta, es decir FrenosBicicleta, SuspencionBicicleta  o VolanteBicicleta,
estos devuelven objetos Bicicleta. Estos objetos los vamos pasando en el 
constructor de cada componente. Cuando hemos acabado de decorar la clase,
hacemos una llamada al método para calcular el costo. Esta llamada irá llamando
a los métodos CalcularCosto de todos los objetos creados para al final calcular
el precio final. 
*/

